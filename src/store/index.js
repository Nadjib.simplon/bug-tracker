import { createStore } from 'vuex'
import firebase from "firebase";
import router from '@/router'

export default createStore({
    state: {
        appShow : false,
        isAuthenticated : false,
        user : null,
        appList : [],
        nb : 0,
        firebaseConfig :
            {
                apiKey: "AIzaSyBG3BDU1gLwTeq-wrnEf0VKf1miR7fbcHk",
                authDomain: "bug-tracker-88cd4.firebaseapp.com",
                projectId: "bug-tracker-88cd4",
                storageBucket: "bug-tracker-88cd4.appspot.com",
                messagingSenderId: "902646952626",
                appId: "1:902646952626:web:26550d02e4a5da5c9a8fab"
            }
    },
    getters:{
        getCurrentUser(state){
            return state.user;
        },

    },
    mutations: {
        checkState(state){
            return state.isAuthenticated
        },
        signOut(state) {
            firebase.auth().signOut().then(() => {
                console.log('Déconnecté')
                router.replace({name: "Login"});
                state.isAuthenticated = false;
                localStorage.setItem('connected','false')
                state.user = null;
            })
        },
        hideShowApp(){
            return 'true';
        },
        initialiseDb(){
            const firebaseConfig =
                {
                    apiKey: "AIzaSyBG3BDU1gLwTeq-wrnEf0VKf1miR7fbcHk",
                    authDomain: "bug-tracker-88cd4.firebaseapp.com",
                    projectId: "bug-tracker-88cd4",
                    storageBucket: "bug-tracker-88cd4.appspot.com",
                    messagingSenderId: "902646952626",
                    appId: "1:902646952626:web:26550d02e4a5da5c9a8fab"
                }
            firebase.initializeApp(firebaseConfig)
        }



    },
    actions: {
        register({commit},user ){

            firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
                .then((user) => {
                    // Signed in
                    router.replace({name : "Login" });   // essayer la methode.push()
                    console.log(user)
                })
        }
    },

    modules: {

    }
})
