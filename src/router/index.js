import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Register from "@/views/Register";
import Login from "@/views/Login";
import store from '@/store';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },{
    path: '/register',
    name: 'Register',
    component: Register


  },{
    path: '/login',
    name: 'Login',
    component: Login

  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  let isAuthenticated = store.state.isAuthenticated

  if ((to.name == 'Login' || to.name == 'Register') && isAuthenticated === false){
    next()}
    else if (isAuthenticated === true){
      next()
      store.commit('checkState')
  }
  else {next ( {name : 'Login'})
      store.commit('checkState')

  }

})

export default router
